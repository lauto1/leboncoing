import { Injectable } from '@angular/core';
import { Status } from '../status.enum';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class AdServiceService {
  counter: number = 0;
  ads: any[];
  url: string = 'https://le-bon-coing.firebaseio.com/';

  constructor(private http: HttpClient) {

  }
  getAds() {
    this.http.get<any>(this.url+'ads.json').subscribe(
      (data) => {
        console.log('données reçues:', data); 
        this.ads = Object.keys(data).map(function (key) {
          return data[key];
        })},
        (error) => console.error('erreur:', error),
          () => console.log('terminé')
    )
  }
  createAd(body) {
    this.http.post<any>(this.url+'ads.json', body).subscribe(
      (data) => console.log('données envoyée:', data),
      (error) => console.error('erreur:', error),
      () => console.log('terminé')
    )
  }
  updateAd() { }
  deleteAll() {
    this.http.delete<any>(this.url+'ads.json').subscribe(
      (data)=> console.log('delete all'),
      (error)=> console.error('erreur :',error),
      ()=>console.log('terminé')
    )
  }
  deleteById() { }

  count(ads) {
    let i = 0;
    ads.forEach(function (obj) {
      if (obj.status === 'Publiée') {
        i++;
      }
    });
    return i;
  }
  onPublishAll() {
    this.ads.map(function (obj) {
      return obj.status = Status.publiée;
    });
  }

  publishById(index: number) {
    this.getAdById(index).status = Status.publiée;
  }

  deactivateById(index: number) {

    this.getAdById(index).status = Status.desactivée;
  }

  tryById(index: number) {

    this.getAdById(index).status = Status.brouillon;
  }

  getAdById(value: number) {
    const ad = this.ads.find(function (elt) {
      console.log(elt.id);
      return elt.id == value;

    });
    return ad;
  }
}
