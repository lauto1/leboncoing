import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
  isAuth:boolean=false;

  constructor() { }

  setAuthTrue(){
    this.isAuth=true;
  }

  setAuthFalse(){
    this.isAuth=false;
  }

}
