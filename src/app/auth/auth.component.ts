import { Component, OnInit } from '@angular/core';
import { AuthGuard } from '../services/auth.guard';
import { AuthService } from '../services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  loginForm:FormGroup ; 
  login = new FormControl('',Validators.required);
  password=new FormControl('',Validators.required);

  constructor(private auth:AuthService, private authGuard:AuthGuard,private router : Router) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      login:new FormControl('',Validators.required),
    password:new FormControl('',Validators.required)});
  }

  onLogin(){
    this.auth.setAuthTrue();
    this.router.navigate(['']);
  }
  
}
