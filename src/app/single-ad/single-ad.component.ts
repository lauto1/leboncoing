import { Component, OnInit,Input } from '@angular/core';
import { AdServiceService } from '../services/ad-service.service';
import { ActivatedRoute } from '@angular/router';
import { Status } from '../status.enum';

@Component({
  selector: 'app-single-ad',
  templateUrl: './single-ad.component.html',
  styleUrls: ['./single-ad.component.scss']
})
export class SingleAdComponent implements OnInit {
  id:number;
  @Input() title: string = 'title';
  @Input() content:string = 'content';
  @Input() status:string = 'status';
  @Input() img:string='img';

  constructor(private adService : AdServiceService, private route: ActivatedRoute) { 

  }

  ngOnInit() {
    this.id =  this.route.snapshot.params['id'];
    this.title = this.adService.getAdById(this.id).title;
    this.content = this.adService.getAdById(this.id).content;
    this.status = this.adService.getAdById(this.id).status;
    this.img = this.adService.getAdById(this.id).img;
    console.log(this.adService.getAdById(this.id));

  }

}
