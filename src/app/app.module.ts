import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { AdComponent } from './ad/ad.component';
import { AdServiceService } from './services/ad-service.service';
import { AuthComponent } from './auth/auth.component';
import { MainViewComponent } from './main-view/main-view.component';
import { Routes, RouterModule } from '@angular/router';
import { CounterComponent } from './counter/counter.component';
import { RoutesErrorComponent } from './routes-error/routes-error.component';
import { SingleAdComponent } from './single-ad/single-ad.component';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth.guard';
import { HttpClientModule } from '@angular/common/http';
import { NewAdComponent } from './new-ad/new-ad.component';

const appRoutes: Routes = [
  {path: 'offres', canActivate: [AuthGuard] ,component: MainViewComponent},
  {path: 'auth', component: AuthComponent },
  {path: '',canActivate: [AuthGuard], component: MainViewComponent},
  {path:'new',component:NewAdComponent},
  {path:'offres/:id',canActivate: [AuthGuard],component:SingleAdComponent},
  {path:'not-found',component: RoutesErrorComponent},
  {path:'**',redirectTo:'not-found'}
];

@NgModule({
  declarations: [
    AppComponent,
    AdComponent,
    AuthComponent,
    MainViewComponent,
    CounterComponent,
    SingleAdComponent,
    RoutesErrorComponent,
    NewAdComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [
    AdServiceService,
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
