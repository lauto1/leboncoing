import { Component, OnInit, Input } from '@angular/core';
import fontawesome from '@fortawesome/fontawesome';
import { AdServiceService } from '../services/ad-service.service';

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss']
})
export class AdComponent implements OnInit {
  @Input() index:number;
  @Input() title :string;
  @Input() status :string;
  @Input() content: string;
  @Input() img:string;
  @Input() id:string;

  constructor(private AdService:AdServiceService) { 
    
  }

  ngOnInit() {
  }

  setColor(){
    if(this.status==='brouillon'){
      return 'grey';
    }else if(this.status==='Publiée'){
      return 'green';
    }else if(this.status==='desactivée'){
      return 'red';
    }
  }
  getColor(){
    if(this.status==='brouillon'){
      return 'lightgrey';
    }else if(this.status==='Publiée'){
      return 'lightgreen';
    }else if(this.status==='desactivée'){
      return '#FF9999';
    }
  }

  onPublish(){
    return this.AdService.publishById(+this.id);
  }
  onDeactivate(){
    return this.AdService.deactivateById(+this.id);
  }
  onTrying(){
    return this.AdService.tryById(+this.id);
  }
}
