import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Status } from '../status.enum';
import { AdServiceService } from '../services/ad-service.service';

@Component({
  selector: 'app-new-ad',
  templateUrl: './new-ad.component.html',
  styleUrls: ['./new-ad.component.scss']
})
export class NewAdComponent implements OnInit {
  addForm: FormGroup;

  constructor(private adService: AdServiceService) { }

  ngOnInit() {
    this.addForm = new FormGroup(
      {
        title : new FormControl('',Validators.required),
        img: new FormControl('',Validators.required),
        status : new FormControl('',Validators.required),
        content: new FormControl('',Validators.required)
      }
    )
  }

  onSubmit(form){
    this.adService.createAd(form.value);
  }

}
