import { Component, OnInit, DoCheck } from '@angular/core';
import { AdServiceService } from '../services/ad-service.service';
import {Input} from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit, DoCheck {
  @Input() ads:any;
  counter: number;

  constructor(private AdService: AdServiceService) { }
  ngOnInit() { }

  ngDoCheck() {
    this.counter = this.AdService.count(this.ads);
  }
}
