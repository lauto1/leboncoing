import { Component, OnInit, DoCheck } from '@angular/core';
import { Status } from '../status.enum';
import { AdServiceService } from '../services/ad-service.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent implements OnInit,DoCheck {
  ads :any;
  isAuth :boolean;

// status:string;
//   enum = Status
  

  constructor(private adService : AdServiceService,private authService:AuthService){

  }
    onPublishAll(){
  return this.adService.onPublishAll();
  }
  
    ngOnInit(){
      this.isAuth = this.authService.isAuth;
      this.adService.getAds();
      
    }
    ngDoCheck(){
      this.ads = this.adService.ads;
    }
    onDisconnect(){
      this.authService.setAuthFalse();
      
    }

}
